# env
Personal environment files for different machines

## How To Use
### First Time Checkout (on a new machine)
```bash
cd      # set working directory to $HOME
git clone git@gitlab.com:marcusljx/env.git
source ~/env/ubuntu
```

### Changing Environments
```bash
source ${ENV}/ubuntu # change to whichever env file you need to use
```

Each file represents type of machine environment it is expected to run on.
Each script has a basic set of assumptions,
such as default tools expected to exist in the environment
(ie, `wget`, `apt`, `snap`, `brew`, etc.)
